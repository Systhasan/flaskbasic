from flask import redirect, url_for, render_template, Blueprint

messages_blueprint = Blueprint('messages', __name__, template_folder='templates')

@messages_blueprint.route('/')
def index():
    return render_template('messages/index.html')
