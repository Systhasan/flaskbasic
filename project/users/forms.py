# ================================================================================
# @type Import
# @name Form, StringField, TextAreaField, PasswordField, validators, SelectField
# @desc Include some of wtforms clases which used for our input field.
# =================================================================================
from wtforms import Form, StringField, TextAreaField, PasswordField, validators, SelectField
from wtforms.fields.html5 import EmailField

# ================================================
# @type Class
# @name UserAddForm
# @params Form Object
# @desc UserAddForm is a class from forms file.
#       This class use for make our input fields
#       and field rules.
# ================================================
class UserAddForm(Form):
    first_name = StringField('First Name', [validators.Length(min=1,max=50)],  render_kw={"placeholder":"First Name"} )
    last_name  = StringField('Last Name', [validators.Length(min=4, max=50)],  render_kw={"placeholder":"Last Name"} )
    email      = EmailField('Email', [validators.DataRequired(), validators.Email()],  render_kw={"placeholder":"Email"})
    # status     = SelectField('Set Status', choices=[('0','Inactive'),('1','Active')])
    details    = StringField('Details', [validators.Length(max=1000)], render_kw={"placeholder":"User Bio Details"})