# ================================================
# @type Import
# @name db, ma
# @desc db database, ma marshmallow objects
# ================================================
from project import db, ma

# Define user model class
class User (db.Model):
    __tablename__   = 'user'
    id              = db.Column(db.Integer, primary_key=True)
    first_name      = db.Column(db.String(62))
    last_name       = db.Column(db.String(62))
    email           = db.Column(db.String(62), unique=True)
    details         = db.Column(db.Text)
    
    def __init__(self, first_name, last_name, email, details):
        self.first_name = first_name
        self.last_name  = last_name
        self.email      = email
        self.details    = details

# =============================================== 
# User Schema
# @desc 
# ===============================================
class UserSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ("id", "first_name", "last_name", "email", "details")
        
# Init DB schema
user_schema  = UserSchema(strict=True)
users_schema = UserSchema(many=True, strict=True)   