# =============================================================================
# @type Import
# @name redirect, Blueprint, render_template, request, url_for, jsonify, flash
# @desc include class, method and defination form flask module
# ==============================================================================
from flask import redirect, Blueprint, render_template, request, url_for, jsonify, flash, session

from project.services.authentication.UsserLogged import login_required
# ================================================
# @type Import
# @name UserAddForm
# @desc UserAddForm is a class from forms file
# ================================================
from project.users.forms import UserAddForm

#from project.models import User

# ================================================
# @type Import
# @name db, ma
# @desc db database, ma marshmallow objects
# ================================================
from project import db, ma

# ================================================
# @type Init
# @name users_blueprint
# @desc init bludeprint class and pass 3 argument like, folder name, templat folder name
# ================================================
users_blueprint = Blueprint('users', __name__, template_folder='templates')

from project.models import User
# Define user model class
# class User (db.Model):
#     __tablename__   = 'user'
#     id              = db.Column(db.Integer, primary_key=True)
#     first_name      = db.Column(db.String(62))
#     last_name       = db.Column(db.String(62))
#     email           = db.Column(db.String(62), unique=True)
#     details         = db.Column(db.Text)
#     type            = db.Column(db.String(16), nullable=True)
#     password        = db.Column(db.String(255), nullable=True)
    
#     def __init__(self, first_name, last_name, email, details):
#         self.first_name = first_name
#         self.last_name  = last_name
#         self.email      = email
#         self.details    = details

# =============================================== 
# User Schema
# @desc 
# ===============================================
# class UserSchema(ma.Schema):
#     class Meta:
#         # Fields to expose
#         fields = ("id", "first_name", "last_name", "email", "details")
        
# Init DB schema
# user_schema  = UserSchema(strict=True)
# users_schema = UserSchema(many=True, strict=True)       

# =============================================== 
# @type Routes
# @request_url  http://localhost:5000/users/
# @request_type GET, POST
# @desc 
# ===============================================
@users_blueprint.route('/', methods=["GET","POST"])
@login_required
def index():
    data = { 'loged_in': True }
    return render_template('users/index.html', data=data, example=User.query.all() )


# =============================================== 
# @type Routes
# @request_url  http://localhost:5000/users/add
# @request_type POST
# @desc using json add an user to the db. 
# ===============================================
@users_blueprint.route('/add', methods=["post"])
@login_required
def add_user():
    first_name = request.json['first_name']
    last_name  = request.json['last_name']
    email      = request.json['email']
    details    = request.json['details']
    
    new_user = User(first_name, last_name, email, details)
    db.session.add(new_user)
    db.session.commit()
    return user_schema.jsonify(new_user)


# =============================================== 
# @type Routes
# @request_url  users/details/<int:id>
# @request_type GET
# @desc 
# ===============================================
@users_blueprint.route('/details/<int:id>')
@login_required
def view_user(id):
    user_all_data = User.query.get(id)
    data = { 'loged_in': True }
    return render_template('users/user_details.html', data=data, user=user_all_data)


# =============================================== 
# @type Routes
# @request_url  users/edit/<int:id>
# @request_type GET
# @desc 
# ===============================================
@users_blueprint.route('/edit/<int:id>')
@login_required
def edit_user(id):
    user_data = User.query.get(id)
    data = { 'loged_in': True }
    return render_template('users/edit_details.html', data=data, user_data=user_data)


# =============================================== 
# @type Routes
# @request_url  users/addnewuser/<int:id>
# @request_type POST, GET
# @desc 
# ===============================================
@users_blueprint.route('/addnewuser', methods=['POST','GET'])
@login_required
def addnewuser():
    # pass request data to the form class
    form = UserAddForm(request.form)
    
    # save form data to the database if its a post request
    if request.method == 'POST' and form.validate():
        first_name = form.first_name.data
        last_name  = form.last_name.data
        email      = form.email.data
        detials    = form.details.data
        
        add_new = User(first_name, last_name, email, detials)
        db.session.add(add_new)
        db.session.commit()
        
        flash('New user add successfully.', 'success')
        redirect( url_for('users.addnewuser') )
                          
    data = { 'loged_in': True }                
    # render page if its a get request
    return render_template('users/add_user.html',data=data, form=form)


# =============================================== 
# @type Routes
# @request_url  users/edituser/<int:id>
# @request_type  GET
# @desc edit an user using its given id. 
# ===============================================  
@users_blueprint.route('/edituser/<int:id>', methods=['POST','GET'])
@login_required
def edituser(id):
    # set form type.
    add_user  = False
    
    # get user data by given user id. 
    user_data = User.query.filter_by(id=id).first()
    
    # pass request data to the form class
    form = UserAddForm(request.form)
    
    if request.method == 'POST' and form.validate():
        user_data.first_name = request.form.get("first_name")
        user_data.last_name = request.form.get("last_name")
        user_data.email = request.form.get("email")
        user_data.details = request.form.get("details")
        db.session.commit()
        # Set flash success message.
        flash('User data update successfully', 'success')
        # redirect_url is positional argument
        # id is keyword arguments need to pass.
        return redirect(url_for('users.edituser', id=user_data.id))
    
    form.first_name.data = user_data.first_name
    form.last_name.data = user_data.last_name
    form.email.data = user_data.email
    form.details.data = user_data.details
    
    data = { 'loged_in': True }
    return render_template('users/add_user.html', 
                           data=data,
                           action="Edit", 
                           add_user = add_user, 
                           form=form)
    
 
# =============================================== 
# @type Routes
# @request_url  users/deleteuser/<int:id>
# @request_type  GET
# @desc delete an user using its given id. 
# ===============================================   
@users_blueprint.route('/deleteuser/<int:id>', methods=['GET'])
def deleteuser(id):
    try:
        user = User.query.filter_by(id=id).first()
        db.session.delete(user)
        db.session.commit()
    except Exception as e:
        print("Failed to add book")
        print(e)
    data = { 'loged_in': True }
    return redirect( url_for('users.index'), data=data )