# ===========================================================
# @type Import
# @name 
# @desc flask_sqlalchemy use for SQL ORM. 
#       flask_marshmallow use for filtering our db table column.
#       flask_modus use for 
# ===========================================================
from flask import Flask, jsonify, render_template, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
# from flask_modus import Modus
from flask_migrate import Migrate

# ===========================================================
# @type Initialize
# @name app
# @desc initialize our flask application to the app variable.
# ===========================================================
app = Flask(__name__)

# ===========================================================
# @type Initialize
# @name ma
# @desc initialize Marshmallow module.
# ===========================================================
ma  = Marshmallow(app)

# ===========================================================
# @type Initialize
# @name modus
# @desc initialize flask_modus module. 
# ===========================================================
# modus = Modus(app)

# ================================================
# @type Assign
# @name app.config['SQLALCHEMY_DATABASE_URI']
# @desc MySql connection. 
# ================================================
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:''@localhost/flask_sqlalchemy'
# Give us SQL Query warning. 
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# set secret key
app.config['SECRET_KEY'] = "secret123456"
app.config['TESTING'] = True

# =================================================
# @type variable
# @name db
# @desc SQLalchemy object assign to the db variable.
#===================================================
db = SQLAlchemy(app)

# migrate = Migrate(app, db)

# Import Blueprint views
from project.login.views import login_blueprint
from project.users.views import users_blueprint
from project.messages.views import messages_blueprint


# Register my Blueprint
app.register_blueprint(login_blueprint, url_prefix='/login')
app.register_blueprint(users_blueprint, url_prefix='/users')
app.register_blueprint(messages_blueprint, url_prefix='/messages')

@app.route('/')
def root():
    data = 'Text form __init__ !!!'
    return data