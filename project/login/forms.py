# ================================================================================
# @type Import
# @name Form, StringField, TextAreaField, PasswordField, validators, SelectField
# @desc Include some of wtforms clases which used for our input field.
# =================================================================================
from wtforms import Form, StringField, PasswordField, validators
from wtforms.fields.html5 import EmailField

# ================================================
# @type Class
# @name UserAddForm
# @params Form Object
# @desc UserAddForm is a class from forms file.
#       This class use for make our input fields
#       and field rules.
# ================================================
class LoginForm(Form):
    email      = EmailField('Email', [validators.DataRequired(), validators.Email()],  render_kw={"placeholder":"Email"})
    password   = PasswordField('Password', [validators.Length(max=1000, min=4)], render_kw={"placeholder":"Login Password"})