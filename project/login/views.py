# =============================================================================
# @type Import
# @name redirect, Blueprint, render_template, request, url_for, jsonify, flash
# @desc include class, method and defination form flask module
# ==============================================================================
from flask import redirect, Blueprint, render_template, request, url_for, jsonify, flash, session

from project.services.authentication.UsserLogged import login_required
# ================================================
# @type Import
# @name UserAddForm
# @desc UserAddForm is a class from forms file
# ================================================
from project.login.forms import LoginForm

# ================================================
# @type Import
# @name db, ma
# @desc db database, ma marshmallow objects
# ================================================
from project import db, ma


# ================================================
# @type Import
# @name User
# @desc import User model class.
# ================================================
from project.users.views import User

# ================================================
# @type Init
# @name users_blueprint
# @desc init bludeprint class and pass 3 argument like, folder name, templat folder name
# ================================================
login_blueprint = Blueprint('login', __name__, template_folder='templates')


@login_blueprint.route('/', methods=['GET', 'POST'])
def index():
    # pass request data to the form class
    form = LoginForm(request.form)
    error = ''
    data  = { 'loged_in': False }
    try: 
        # user make a login request.
        if request.method == "POST" and form.validate():
            # get user data using email and password. 
            user_data = User.query.filter_by(email=form.email.data, password=form.password.data).first()
            # if user are exists in database
            if hasattr(user_data, 'id') :
                flash("You are logged in.")
                session['logged_id']  = True
                # session['login_info'] = user_data    
                return redirect( url_for('users.index') )
                
            else:
                flash("Invalid email or password. Try again.", "danger")
    except Exception as e:
        flash("Unable to find account", "danger")
    return render_template("login/index.html", data=data, error=error, form=form)



@login_blueprint.route("/logout")
@login_required
def logout():
    session.clear()
    flash("You have been logged out!", "success")
    return redirect(url_for('login.index'))

