from flask import redirect, render_template, request, url_for, flash, session
from functools import wraps

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        # if 'loged_in' in session:
        try:
            if session['logged_id']:
                return f(*args, **kwargs)
            else:
                flash("You need to login first.", "danger")
                return redirect(url_for('login.index'))
        except Exception as e:
            print(e)
            flash("You need to login first.", "danger")
            return redirect(url_for('login.index'))
    return wrap

    
def goToHome(f):
    @wraps(f)
    def go_to_home(*args, **kwargs):
        status = True
        try:
            session['logged_id']
        except NameError:
            status = False

        if status:
            return redirect(url_for('users.index'))
        else:
            return redirect(url_for('login.index'))
        
    return go_to_home