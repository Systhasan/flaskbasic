# ================================================
# @type Import
# @name db, ma
# @desc db database, ma marshmallow objects
# ================================================
from project import db

# ================================================
# @type     Class
# @name     User
# @params   {string} first name, 
#           {string} last name, 
#           {string} email, 
#           {string} details
# @desc     User model class
# ================================================
class User (db.Model):
    __tablename__   = 'user'
    id              = db.Column(db.Integer, primary_key=True)
    first_name      = db.Column(db.String(62))
    last_name       = db.Column(db.String(62))
    email           = db.Column(db.String(62), unique=True)
    details         = db.Column(db.Text)
    type            = db.Column(db.String(16), nullable=True)
    password        = db.Column(db.String(255), nullable=True)
    
    def __init__(self, first_name, last_name, email, details):
        self.first_name = first_name
        self.last_name  = last_name
        self.email      = email
        self.details    = details